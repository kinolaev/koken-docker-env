FROM php:5.6.40-apache

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y graphicsmagick imagemagick ffmpeg libmcrypt4 libmcrypt-dev libmagickwand-dev && \
    docker-php-ext-install -j$(nproc) exif mcrypt mysqli && pecl install imagick-3.4.4 && docker-php-ext-enable imagick && \
    apt-get purge -y libmcrypt-dev libmagickwand-dev && apt-get --purge autoremove -y && rm -rf /var/lib/apt/lists/* && \
    mv $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini && \
    sed -ri -e 's!LogFormat "%h!LogFormat "%a!g' /etc/apache2/apache2.conf && \
    sed -ri -e 's!Listen 80!Listen 8080!g' /etc/apache2/ports.conf && \
    sed -ri -e 's!<VirtualHost \*:80>!<VirtualHost *:8080>!g' /etc/apache2/sites-available/000-default.conf && \
    echo 'RemoteIPHeader X-Real-IP\nRemoteIPTrustedProxy 10.0.0.0/8\nRemoteIPTrustedProxy 172.16.0.0/12\nRemoteIPTrustedProxy 192.168.0.0/16' \
    >> /etc/apache2/conf-available/remoteip.conf && \
    a2enmod expires headers remoteip rewrite && a2enconf remoteip

USER www-data

EXPOSE 8080
